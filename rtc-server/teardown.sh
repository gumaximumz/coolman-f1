

MY_INSTANCE_NAME="gcoolman-rtc"
ZONE=asia-southeast1-b

gcloud compute instances delete $MY_INSTANCE_NAME \
    --zone=$ZONE --delete-disks=all

gcloud compute firewall-rules delete default-allow-http-8080
