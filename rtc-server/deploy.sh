# [START getting_started_gce_create_instance]
MY_INSTANCE_NAME="gcoolman-rtc"
ZONE=asia-southeast1-b

gcloud compute instances create $MY_INSTANCE_NAME \
    --image-family=debian-9 \
    --image-project=debian-cloud \
    --machine-type=g1-small \
    --scopes userinfo-email,cloud-platform \
    --metadata-from-file startup-script=startup-script.sh \
    --zone $ZONE \
    --tags http-server
# [END getting_started_gce_create_instance]
