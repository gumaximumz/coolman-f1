# [START getting_started_rtc-server_startup_script]
# Install Stackdriver logging agent
curl -sSO https://dl.google.com/cloudagents/install-logging-agent.sh
bash install-logging-agent.sh
# Install or update needed software
apt-get update
apt-get install -yq git supervisor python python-pip
apt-get install python3-numpy python3-cffi python3-aiohttp \
    libavformat-dev libavcodec-dev libavdevice-dev libavutil-dev \
    libswscale-dev libswresample-dev libavfilter-dev libopus-dev \
    libvpx-dev pkg-config libsrtp2-dev python3-opencv pulseaudio
#apt install libavdevice-dev libavfilter-dev libopus-dev libvpx-dev pkg-config
#apt install libsrtp2-dev
pip install --upgrade pip virtualenv
pip3 install --upgrade pip virtualenv
# Account to own server process
useradd -m -d /home/pythonapp pythonapp
# Fetch source code
#export HOME=/root
git clone https://gitlab.com/gumaximumz/coolman-f1.git /opt/app
# Python environment setup
virtualenv -p python3 /opt/app/rtc-server/venv
source /opt/app/rtc-server/venv/bin/activate
/opt/app/rtc-server/venv/bin/pip install -r /opt/app/rtc-server/requirements.txt
# Set ownership to newly created account
chown -R pythonapp:pythonapp /opt/app
# Put supervisor configuration in proper place
cp /opt/app/rtc-server/python-app.conf /etc/supervisor/conf.d/python-app.conf
# Start service via supervisorctl
supervisorctl reread
supervisorctl update
# [END getting_started_rtc-server_startup_script]
